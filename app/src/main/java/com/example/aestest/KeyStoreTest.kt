package com.example.aestest

import android.util.Log
import java.io.*
import java.security.KeyStore
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.CipherOutputStream
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec

class KeyStoreTest(keyStoreFile: File) {

    @Suppress("UNUSED")
    companion object {
        const val TAG = "KeyStoreTest"

        private const val testText = "Lorem Ipsum jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym."
        private const val testText2 = "Lo"
    }


    private val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")

    init {
        val textToTest = testText

        val password = "mobilny1212"
        val alias = "mobilny"

        val keyAES = readAESKey(keyStoreFile, password, alias)
        //TODO: 08.09.2019 ernest Zapisac klucz AES
        val deleted = keyStoreFile.delete()
        Log.d(TAG, "key -> $keyAES deleted after import -> $deleted")

        /*
         * Every iv can be used only once. Has to be stored aside to cipher text.
         */
        val iv = generateIv()

        //Inicjalizacja algorytmu
        val encryptedData = encrypt(textToTest.toByteArray(), keyAES, iv)
        Log.d(TAG, "encrypted size -> " + encryptedData.size + " value -> " + String(encryptedData))

        //Odszyfrowanie
        val decrypted = decrypt(encryptedData, keyAES, iv)
        Log.d(TAG, "decryptedString -> $decrypted")
    }

    private fun generateIv(): ByteArray {
        val random = SecureRandom()
        return random.generateSeed(16)
    }

    private fun readAESKey(keyStoreFile: File, password: String, alias: String): SecretKey {
        val passwordChars = password.toCharArray()

        val inputStream = keyStoreFile.inputStream()
        val keyStore = KeyStore.getInstance("BKS")
        keyStore.load(inputStream, passwordChars)

        return keyStore.getKey(alias, passwordChars) as SecretKey
    }

    private fun encrypt(data: ByteArray, key: SecretKey, iv: ByteArray): ByteArray {
        val ivSpec = IvParameterSpec(iv)
        cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec)

        val outputStream = ByteArrayOutputStream(data.size)
        val cipherOutputStream = CipherOutputStream(outputStream, cipher)
        cipherOutputStream.write(data)
        cipherOutputStream.flush()
        cipherOutputStream.close()
        return outputStream.toByteArray()
    }

    private fun decrypt(encryptedData: ByteArray, key: SecretKey, iv: ByteArray): String {
        val ivSpec = IvParameterSpec(iv)
        cipher.init(Cipher.DECRYPT_MODE, key, ivSpec)

        val inputStream = ByteArrayInputStream(encryptedData)
        val cipherInputStream = CipherInputStream(inputStream, cipher)

        val inputStreamReader = InputStreamReader(cipherInputStream)
        val reader = BufferedReader(inputStreamReader)
        return reader.readLine()
    }

}