package com.example.aestest

import android.Manifest
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import java.io.File

class MainActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_PERMISSIONS_CODE = 34
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        requestPermissions()

        val bksName = "aes256.bks"
        getBksFile(bksName)?.also { bksFile ->
            val keyStore = KeyStoreTest(bksFile)
            Log.d(KeyStoreTest.TAG, "files -> $bksFile keyStore -> $keyStore")
        }
    }

    private fun getBksFile(bksName: String) : File? {
        val files = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).listFiles()
        return files.firstOrNull { file ->
            file.name == bksName
        }
    }

    private fun requestPermissions() {
        requestPermissions(
                arrayOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                ), REQUEST_PERMISSIONS_CODE
        )
    }

}